import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CampusBinhai from '../views/CampusBinhai.vue'
import CampusChashan from '../views/CampusChashan.vue'
import CampusYueqing from '../views/CampusYueqing.vue'
import StudentSystem from '../views/StudentSystem.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/binhai',
    name: 'binhai',
    component:CampusBinhai
  },
  {
    path: '/chashan',
    name: 'chashan',
    component:CampusChashan
  },
  {
    path: '/login',
    name: 'login',
    component:Login
  },
  {
    path: '/yueqing',
    name: 'yueqing',
    component:  function () {
      return import(/* webpackChunkName: "about" */ '../views/CampusYueqing.vue')
    }
  },
  {
    path: '/student',
    name: 'student',
    component:StudentSystem
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
